import "./App.scss";
import axios from "axios";
import { useState } from "react";

function App() {
  const [job, setJob] = useState("");

  // ! Formni dastlabki value-lari
  const intialValues = {
    name: "",
    surname: "",
    phoneNumber: "",
    email: "",
    dateOfBirth: "",
  };
  const [values, setValues] = useState(intialValues);

  // console.log(values.name);

  const getJobs = () => {
    axios
      .get("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types")
      .then((res) => {
        // setUsers(res.data);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    console.log("submit");
    const formData = {
      // username: values.username,
      // email: values.email,
      // password: values.password,
      // confirmPassword: values.confirmPassword,
      ...values, // ! Osonro yoli lekin initialValues bilan backenddagi value-lari bir xil bo'lishi kerak (username === username)
    };
    // ! axios.post() yangi user qo'shish uchun
    axios
      .post("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/user", formData)
      .catch((err) => console.log("err", err))
      .finally(() => {
        // getUsers();
        setValues(intialValues);
      });
  };

  const onSubmit2 = (e) => {
    e.preventDefault();
    console.log("submit2");
    axios
      .post("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types", job)
      .catch((err) => console.log("err", err))
      .finally(() => {
        getJobs();
        // setJob("");
      });
  };

  return (
    <div className="App">
      <form className="form-control" onSubmit={onSubmit}>
        <div className="wrap">
          <div className="users__info">
            <h1 className="title">User's info</h1>

            <label htmlFor="name" className="name">
              <span>name</span>
              <input
                onChange={(e) => setValues({ ...values, name: e.target.value })}
                type="text"
                name="name"
                id="name"
              />
            </label>

            <label htmlFor="surname" className="surname">
              <span>surname</span>
              <input
                onChange={(e) =>
                  setValues({ ...values, surname: e.target.value })
                }
                type="text"
                name="surname"
                id="surname"
              />
            </label>

            <label htmlFor="phoneNumber" className="phoneNumber">
              <span>phone number</span>
              <input
                onChange={(e) =>
                  setValues({ ...values, phoneNumber: e.target.value })
                }
                type="text"
                name="phoneNumber"
                id="phoneNumber"
              />
            </label>

            <label htmlFor="email" className="email">
              <span>email</span>
              <input
                onChange={(e) =>
                  setValues({ ...values, email: e.target.value })
                }
                type="text"
                name="email"
                id="email"
              />
            </label>

            <label htmlFor="date" className="date">
              <span>date of birth</span>
              <input
                onChange={(e) => setValues({ ...values, date: e.target.value })}
                type="date"
                name="date"
                id="date"
              />
            </label>
          </div>

          <div className="rightDivWrapper">
            <div className="work__details">
              <h1 className="title">work details</h1>

              <label htmlFor="companyName" className="companyName">
                <span>company name</span>
                <input type="text" name="companyName" id="companyName" />
              </label>

              <div className="select-wrapper">
                <label htmlFor="jobType" className="jobType select">
                  <span>job type</span>
                  <select name="jobType" id="jobType">
                    <option value="test1">test1</option>
                    <option value="test2">test2</option>
                  </select>
                </label>

                <label htmlFor="experience" className="experience select">
                  <span>experience</span>
                  <select name="experience" id="experience">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                </label>
              </div>
            </div>

            <form className="form__jobType" onSubmit={onSubmit2}>
              <h1 className="title">job type</h1>

              <label htmlFor="addJobType" className="addJobType">
                <span>label (addJobType) </span>
                <input
                  onChange={(e) => setJob({ job: e.target.value })}
                  type="text"
                  name="addJobType"
                  id="addJobType"
                />
              </label>

              <div className="btns__wrapper">

                <button className="btn cancel">Отменить</button>
                <button type="submit" className="btn save">Сохранить</button>
              </div>
            </form>
          </div>
        </div>

        <div className="btns__wrapper">
      <a className="repo" href="https://gitlab.com/izzatazimov958/react-forms-with-axios_task">gitlab repo</a>

          <button className="btn cancel">Отменить</button>
          <button type="submit" className="btn save">Сохранить</button>
        </div>
      </form>

      <div className="form-output">
        <div className="usersListDiv">
          <h1 className="title">User Lists</h1>
          <ul className="users__list">
            {/* labels */}
            <li className="labels">
              <span className="numberCell">№</span>
              <span className="fullNameCell">Full name</span>
              <span className="dateOfBirthCell">Date of Birth</span>
              <span className="phoneCell">phone</span>
              <span className="emailCell">email</span>
              <span className="companyNameCell">company name</span>
              <span className="jobTypeCell">job type</span>
              <span className="experienceCell">experience</span>
              <span className="emptyCell"></span>
            </li>

            {/* list */}
            <li className="users__list-item">
              <span className="numberCell">1</span>
              <span className="fullNameCell">Dena Hilll</span>
              <span className="dateOfBirthCell">03.03.1996</span>
              <span className="phoneCell">+ 998 90 123 45 67</span>
              <span className="emailCell">Denahill@gmail.com</span>
              <span className="companyNameCell">Amazon</span>
              <span className="jobTypeCell">Developer</span>
              <span className="experienceCell">5</span>
              <span className="emptyCell remove">
                <i className="fa-solid fa-trash-can"></i>
              </span>
            </li>

            <li className="users__list-item">
              <span className="numberCell">1</span>
              <span className="fullNameCell">Dena Hilll</span>
              <span className="dateOfBirthCell">03.03.1996</span>
              <span className="phoneCell">+ 998 90 123 45 67</span>
              <span className="emailCell">Denahill@gmail.com</span>
              <span className="companyNameCell">Amazon</span>
              <span className="jobTypeCell">Developer</span>
              <span className="experienceCell">5</span>
              <span className="emptyCell remove">
                <i className="fa-solid fa-trash-can"></i>
              </span>
            </li>
          </ul>
        </div>

        <div className="jobTypeDiv">

          <h1 className="title">job type</h1>
          <ul className="jobType__wrapper">
            <li className="jobType__wrapper-label">
              <span className="numberCell">№</span>
              <span className="label">Label</span>
              <span className="emptyCell"></span>
            </li>

            <li className="jobType__wrapper-item">
              <span className="numberCell">1</span>
              <span className="label">developer</span>
              <span className="emptyCell remove">
                <i className="fa-solid fa-trash-can"></i>
              </span>
            </li>
            <li className="jobType__wrapper-item">
              <span className="numberCell">1</span>
              <span className="label">developer</span>
              <span className="emptyCell remove">
                <i className="fa-solid fa-trash-can"></i>
              </span>
            </li>
            <li className="jobType__wrapper-item">
              <span className="numberCell">1</span>
              <span className="label">developer</span>
              <span className="emptyCell remove">
                <i className="fa-solid fa-trash-can"></i>
              </span>
            </li>
          </ul>
        </div>
      </div>

    </div>
  );
}

export default App;
